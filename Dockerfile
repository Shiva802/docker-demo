
# Use an official Node.js image as the base image
FROM node:latest

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json into the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code into the container
COPY . .

# Expose port 3000 to the outside world
EXPOSE 3000

# Install nodemon 
RUN npm install -g nodemon

# Define the command to run the application 
CMD ["nodemon","--legacy-watch","--inspect=0.0.0.0:9229","index.js"] 